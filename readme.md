### Requisitos

- [x] API REST

Para cada planeta, os seguintes dados devem ser obtidos do banco de dados da aplicação, sendo inserido manualmente:

- [x] Nome
- [x] Clima
- [x] Terreno
- [x] Quantidade de aparições em filmes (https://swapi.co/)

Funcionalidades desejadas: 
- [x] Adicionar um planeta (com nome, clima e terreno)
- [x] Listar planetas
- [x] Buscar por nome
- [x] Buscar por ID
- [x] Remover planeta

### Frameworks
- Web: Echo (https://echo.labstack.com/)
- Tests: github.com/stretchr/testify/

### Banco
- MongoDB

### Como rodar

```shell
$ MONGODB_URI=mongodb://localhost:27017/exampleapi SWAPI_URL=https://swapi.co/api go run main.go
```

### Testes

- Manuais: https://www.getpostman.com/collections/38c4f9ff56028e90d5b0
- Automaticos: `$ go test -v ./...`
