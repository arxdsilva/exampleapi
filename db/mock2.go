package db

import (
	"encoding/json"
	"errors"

	"gopkg.in/mgo.v2"
)

// MockSession satisfies Session
type MockSession2 struct{}

func NewMockSession2() Session                   { return MockSession2{} }
func (fs MockSession2) Close()                   {}
func (fs MockSession2) DB(name string) DataLayer { return MockDatabase2{} }

// MockDatabase satisfies DataLayer
type MockDatabase2 struct{}

func (db MockDatabase2) C(name string) Collection { return MockCollection2{} }

// MockCollection2 satisfies Collection
type MockCollection2 struct{}

func (fc MockCollection2) Find(query interface{}) (q *mgo.Query)                       { return }
func (fc MockCollection2) Insert(docs ...interface{}) (err error)                      { return }
func (fc MockCollection2) Update(selector interface{}, update interface{}) (err error) { return }
func (fc MockCollection2) Remove(selector interface{}) (err error)                     { return }

func (fc MockCollection2) FindAll(result interface{}) (err error) {
	json.Unmarshal([]byte(`[]`), result)
	return
}

func (fc MockCollection2) FindByID(id int, result interface{}) (err error) {
	json.Unmarshal([]byte(`{}`), result)
	return
}

func (fc MockCollection2) FindByName(name string, result interface{}) (err error) {
	r := []byte(`{"name":"Tatooine2", "url":"https://example.com"}`)
	json.Unmarshal(r, result)
	return
}

func (fc MockCollection2) Delete(id int) (err error) {
	return errors.New("internal error")
}
