package db

import (
	"encoding/json"

	"gopkg.in/mgo.v2"
)

// MockSession satisfies Session
type MockSession struct{}

func NewMockSession() Session                   { return MockSession{} }
func (fs MockSession) Close()                   {}
func (fs MockSession) DB(name string) DataLayer { return MockDatabase{} }

// MockDatabase satisfies DataLayer
type MockDatabase struct{}

func (db MockDatabase) C(name string) Collection { return MockCollection{} }

// MockCollection satisfies Collection
type MockCollection struct{}

func (fc MockCollection) Find(query interface{}) (q *mgo.Query)                       { return }
func (fc MockCollection) Insert(docs ...interface{}) (err error)                      { return }
func (fc MockCollection) Update(selector interface{}, update interface{}) (err error) { return }
func (fc MockCollection) Delete(id int) (err error)                                   { return }
func (fc MockCollection) Remove(selector interface{}) (err error)                     { return }

func (fc MockCollection) FindAll(result interface{}) (err error) {
	r := []byte(`[{"name":"Tatooine"}]`)
	json.Unmarshal(r, result)
	return
}

func (fc MockCollection) FindByID(id int, result interface{}) (err error) {
	r := []byte(`{"name":"Tatooine", "url":"https://example.com"}`)
	json.Unmarshal(r, result)
	return
}

func (fc MockCollection) FindByName(name string, result interface{}) (err error) { return }
