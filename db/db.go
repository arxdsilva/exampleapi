package db

import (
	"log"
	"os"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Collection interface
type Collection interface {
	Find(query interface{}) *mgo.Query
	FindByID(id int, result interface{}) (err error)
	FindByName(name string, result interface{}) (err error)
	FindAll(result interface{}) (err error)
	Insert(docs ...interface{}) error
	Remove(selector interface{}) error
	Delete(id int) error
	Update(selector interface{}, update interface{}) error
}
type Session interface {
	DB(name string) DataLayer
	Close()
}

type MongoSession struct {
	*mgo.Session
}

// DB shadows *mgo.DB
func (s MongoSession) DB(name string) DataLayer {
	return &MongoDatabase{Database: s.Session.DB(name)}
}

// DataLayer to access the database struct
type DataLayer interface {
	C(name string) Collection
}

// NewSession a new Mongo Session.
func NewSession() Session {
	mgoSession, err := mgo.Dial(os.Getenv("MONGODB_URI"))
	if err != nil {
		log.Fatal("Could not connect to MongoDB: ", err)
	}
	return MongoSession{mgoSession}
}

type MongoCollection struct {
	*mgo.Collection
}

func (c MongoCollection) FindByID(id int, result interface{}) (err error) {
	return c.Find(bson.M{"swid": id}).One(result)
}

func (c MongoCollection) FindByName(name string, result interface{}) (err error) {
	return c.Find(bson.M{"name": name}).One(result)
}

func (c MongoCollection) FindAll(result interface{}) (err error) {
	return c.Find(bson.M{}).All(result)
}

func (c MongoCollection) Delete(id int) (err error) {
	return c.Remove(bson.M{"swid": id})
}

// MongoDatabase wraps mgo.Database
type MongoDatabase struct {
	*mgo.Database
}

// C shadows *mgo.DB to
func (d MongoDatabase) C(name string) Collection {
	return &MongoCollection{Collection: d.Database.C(name)}
}
