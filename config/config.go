package config

import (
	"github.com/arxdsilva/exampleapi/db"
	"github.com/arxdsilva/exampleapi/models"
)

func Load() {
	models.DB = db.NewSession().DB("exampleapi")
}

func LoadMock() {
	models.DB = db.NewMockSession().DB("exampleapi")
}
