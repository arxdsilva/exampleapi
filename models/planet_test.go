package models

import (
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// checkar com diversos climas/terrenos
func TestPlanet_Check_OK(t *testing.T) {
	p := Planet{
		Name:    "Utapau",
		Climate: "arid",
		Terrain: "rocky",
	}
	err := p.Check()
	assert.Nil(t, err)
}

func TestPlanet_Check_NameErr(t *testing.T) {
	p := Planet{
		Climate: "arid",
		Terrain: "rocky",
	}
	err := p.Check()
	assert.NotNil(t, err)
	assert.True(t, strings.Contains(err.Error(), "have a name"))
}

func TestPlanet_Check_Name_NotExist(t *testing.T) {
	p := Planet{
		Name:    "Charizard",
		Climate: "arid",
		Terrain: "rocky",
	}
	err := p.Check()
	assert.NotNil(t, err)
	assert.True(t, strings.Contains(err.Error(), "name does not exist"))
}

func TestPlanet_Check_ClimateErr(t *testing.T) {
	p := Planet{
		Name:    "Utapau",
		Terrain: "rocky",
	}
	err := p.Check()
	assert.NotNil(t, err)
	assert.True(t, strings.Contains(err.Error(), "have a climate"))
}

func TestPlanet_Check_TerrainErr(t *testing.T) {
	p := Planet{
		Name:    "Utapau",
		Climate: "arid",
	}
	err := p.Check()
	assert.NotNil(t, err)
	assert.True(t, strings.Contains(err.Error(), "have a terrain"))
}

func TestPlanet_Check_MultClimates_OK(t *testing.T) {
	p := Planet{
		Name:    "Utapau",
		Climate: "arid, artic",
		Terrain: "rocky",
	}
	err := p.Check()
	assert.Nil(t, err)
}

func TestPlanet_Check_MultClimates_err(t *testing.T) {
	p := Planet{
		Name:    "Utapau",
		Climate: "arid, err",
		Terrain: "rocky",
	}
	err := p.Check()
	assert.NotNil(t, err)
	assert.True(t, strings.Contains(err.Error(), "not registered for planet"))
}

func TestPlanet_Check_MultTerrains_err(t *testing.T) {
	p := Planet{
		Name:    "Utapau",
		Climate: "arid",
		Terrain: "swamp, err",
	}
	err := p.Check()
	assert.NotNil(t, err)
	assert.True(t, strings.Contains(err.Error(), "not registered for planet"))
}

func TestPlanet_FetchPlanetInfos(t *testing.T) {
	planetID := "1"
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/planets/"+planetID+"/" {
				w.Header().Add("Content-Type", "application/json")
				json := `{
					"name": "Tatooine",
					"rotation_period": "23",
					"orbital_period": "304",
					"diameter": "10465",
					"climate": "arid",
					"gravity": "1 standard",
					"terrain": "desert",
					"surface_water": "1",
					"population": "200000",
					"residents": ["https://swapi.co/api/people/1/", "https://swapi.co/api/people/2/", "https://swapi.co/api/people/4/", "https://swapi.co/api/people/6/", "https://swapi.co/api/people/7/", "https://swapi.co/api/people/8/", "https://swapi.co/api/people/9/", "https://swapi.co/api/people/11/", "https://swapi.co/api/people/43/", "https://swapi.co/api/people/62/"],
					"films": ["https://swapi.co/api/films/5/", "https://swapi.co/api/films/4/", "https://swapi.co/api/films/6/", "https://swapi.co/api/films/3/", "https://swapi.co/api/films/1/"],
					"created": "2014-12-09T13:50:49.641000Z",
					"edited": "2014-12-21T20:48:04.175778Z",
					"url": "https://swapi.co/api/planets/1/"
				}`
				w.Write([]byte(json))
			}
		}),
	)
	os.Setenv("SWAPI_URL", ts.URL)
	// os.Setenv("SWAPI_URL", "https://swapi.co/api")
	defer os.Unsetenv("SWAPI_URL")
	p := Planet{
		Name:    "Tatooine",
		Climate: "arid",
		Terrain: "swamp",
	}
	err := p.Check()
	assert.Nil(t, err)
	err = p.FetchPlanetInfos()
	assert.Nil(t, err)
	assert.Equal(t, "23", p.RotationPeriod)
	assert.Equal(t, "304", p.OrbitalPeriod)
	assert.Equal(t, "10465", p.Diameter)
	assert.Equal(t, "1 standard", p.Gravity)
	assert.Equal(t, "1", p.SurfaceWater)
	assert.Equal(t, "200000", p.Population)
	assert.NotEmpty(t, p.ResidentURLs)
	assert.NotEmpty(t, p.FilmURLs)
	assert.Equal(t, "https://swapi.co/api/planets/1/", p.URL)
}
