package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/arxdsilva/exampleapi/db"
	"github.com/google/uuid"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
)

var DB db.DataLayer

const collection = "planets"

// these are 'good' evils, help reduce complexity in favor of faster development
// (avoids complex SWAPI calls)
// but in a bigger environment they would become collections
var registeredClimates = map[string]string{"arid": "", "artic": "", "artificialtemperate": "", "frigid": "", "frozen": "", "hot": "", "humid": "", "moist": "", "murky": "", "polluted": "", "rocky": "", "subartic": "", "superheated": "", "temperate": "", "tropical": "", "unknown": "", "windy": ""}
var registeredTerrains = map[string]string{"rocky": "", "swamp": "", "gasgiant": "", "scrublands": "", "grass": "", "rockycanyons": "", "rainforests": "", "mountain ranges": "", "ocean": "", "cities": "", "forests": "", "swamps": "", "mesas": "", "vines": "", "cliffs": "", "mountains": "", "desert": "", "fields": "", "rockyislands": "", "unknown": "", "cityscape": "", "mountain": "", "fungusforests": "", "oceans": "", "acid pools": "", "ice caves": "", "glaciers": "", "savannas": "", "savannahs": "", "sinkholes": "", "airlessasteroid": "", "tundra": "", "grassyhills": "", "barren": "", "seas": "", "deserts": "", "lakes": "", "caves": "", "rivers": "", "rock arches": "", "plains": "", "islands": "", "ash": "", "toxiccloudsea": "", "bogs": "", "savanna": "", "canyons": "", "volcanoes": "", "hills": "", "rocky deserts": "", "grasslands": "", "jungle": "", "verdant": "", "plateaus": "", "rock": "", "lava rivers": "", "ice canyons": "", "urban": "", "reefs": "", "jungles": "", "valleys": ""}
var registeredNames = map[string]int{"Utapau": 12, "Toydaria": 34, "Vulpter": 39, "Tatooine": 1, "Coruscant": 9, "Shili": 58, "Stewjon": 20, "Glee Anselm": 44, "Zolan": 54, "Kalee": 59, "Socorro": 30, "Quermia": 48, "Dorin": 49, "Bespin": 6, "Kashyyyk": 14, "Felucia": 17, "Umbara": 60, "Geonosis": 11, "unknown": 28, "Malastare": 35, "Concord Dawn": 53, "Ojom": 55, "Serenno": 52, "Endor": 7, "Bestine IV": 26, "Chandrila": 32, "Ryloth": 37, "Skako": 56, "Troiken": 40, "Yavin IV": 3, "Kamino": 10, "Eriadu": 21, "Sullust": 33, "Aleen Minor": 38, "Nal Hutta": 24, "Dantooine": 25, "Tund": 41, "Naboo": 8, "Saleucami": 19, "Mon Cala": 31, "Mirial": 51, "Mustafar": 13, "Polis Massa": 15, "Trandosha": 29, "Champala": 50, "Corellia": 22, "Cerea": 43, "Tholoth": 46, "Jakku": 61, "Iktotch": 47, "Hoth": 4, "Dagobah": 5, "Cato Neimoidia": 18, "Dathomir": 36, "Haruun Kal": 42, "Alderaan": 2, "Rodia": 23, "Mygeeto": 16, "Ord Mantell": 27, "Iridonia": 45, "Muunilinst": 57}
var registeredIDs = map[int]string{8: "Naboo", 30: "Socorro", 35: "Malastare", 57: "Muunilinst", 6: "Bespin", 16: "Mygeeto", 41: "Tund", 13: "Mustafar", 22: "Corellia", 24: "Nal Hutta", 52: "Serenno", 7: "Endor", 47: "Iktotch", 55: "Ojom", 1: "Tatooine", 49: "Dorin", 61: "Jakku", 3: "Yavin IV", 18: "Cato Neimoidia", 38: "Aleen Minor", 2: "Alderaan", 4: "Hoth", 39: "Vulpter", 54: "Zolan", 56: "Skako", 19: "Saleucami", 20: "Stewjon", 28: "unknown", 36: "Dathomir", 50: "Champala", 60: "Umbara", 31: "Mon Cala", 33: "Sullust", 40: "Troiken", 45: "Iridonia", 59: "Kalee", 58: "Shili", 5: "Dagobah", 9: "Coruscant", 26: "Bestine IV", 32: "Chandrila", 46: "Tholoth", 12: "Utapau", 17: "Felucia", 34: "Toydaria", 42: "Haruun Kal", 43: "Cerea", 21: "Eriadu", 29: "Trandosha", 48: "Quermia", 53: "Concord Dawn", 10: "Kamino", 11: "Geonosis", 15: "Polis Massa", 25: "Dantooine", 37: "Ryloth", 14: "Kashyyyk", 23: "Rodia", 27: "Ord Mantell", 44: "Glee Anselm", 51: "Mirial"}

type Planet struct {
	SWID           int      `bson:"swid" json:"id"`
	UUID           string   `bson:"uuid" json:"uuid,omitempty"`
	Name           string   `bson:"name" json:"name"`
	Climate        string   `bson:"climate" json:"climate"`
	Terrain        string   `bson:"terrain" json:"terrain"`
	RotationPeriod string   `bson:"rotation_period" json:"rotation_period"`
	OrbitalPeriod  string   `bson:"orbital_period" json:"orbital_period"`
	Diameter       string   `bson:"diameter" json:"diameter"`
	Gravity        string   `bson:"gravity" json:"gravity"`
	SurfaceWater   string   `bson:"surface_water" json:"surface_water"`
	Population     string   `bson:"population" json:"population"`
	ResidentURLs   []string `bson:"residents" json:"residents"`
	FilmURLs       []string `bson:"films" json:"films"`
	URL            string   `bson:"url" json:"url"`
}

func (p *Planet) Check() (err error) {
	if p.Name == "" {
		return errors.New("Planet needs to have a name")
	}
	if p.Climate == "" {
		return errors.New("Planet needs to have a climate")
	}
	if p.Terrain == "" {
		return errors.New("Planet needs to have a terrain")
	}
	var climatesGiven []string
	climatesGiven = append(climatesGiven, strings.Split(p.Climate, ",")...)
	ok, errS := allListInMap(p.Name, "Climate", climatesGiven, registeredClimates)
	if !ok {
		return errors.New(errS)
	}
	var terrainsGiven []string
	terrainsGiven = append(terrainsGiven, strings.Split(p.Terrain, ",")...)
	ok, errS = allListInMap(p.Name, "Terrain", terrainsGiven, registeredTerrains)
	if !ok {
		return errors.New(errS)
	}
	_, ok = registeredNames[p.Name]
	if !ok {
		return errors.New("Planet name does not exist")
	}
	return
}

func (p *Planet) FetchPlanetInfos() (err error) {
	pID := registeredNames[p.Name]
	swapiURL := os.Getenv("SWAPI_URL")
	url := fmt.Sprintf(swapiURL+"/planets/%v/", pID)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	bPlanet := Planet{}
	err = json.NewDecoder(resp.Body).Decode(&bPlanet)
	if err != nil {
		return err
	}
	p.SWID = pID
	p.RotationPeriod = bPlanet.RotationPeriod
	p.OrbitalPeriod = bPlanet.OrbitalPeriod
	p.Diameter = bPlanet.Diameter
	p.Gravity = bPlanet.Gravity
	p.SurfaceWater = bPlanet.SurfaceWater
	p.Population = bPlanet.Population
	p.ResidentURLs = bPlanet.ResidentURLs
	p.FilmURLs = bPlanet.FilmURLs
	p.URL = bPlanet.URL
	return
}

func (p *Planet) ExistsID() bool {
	_, ok := registeredIDs[p.SWID]
	return ok
}

func (p *Planet) ExistsName() bool {
	_, ok := registeredNames[p.Name]
	return ok
}

func (p *Planet) GetAll() (ps []Planet, err error) {
	ps = []Planet{}
	return ps, DB.C(collection).FindAll(&ps)
}

func (p *Planet) GetByID() (pl *Planet, err error) {
	// usando o id da api do SW, mas no mundo real o certo eh usar o uuid que esta no objeto
	// assim evitamos possiveis problemas de seguranca. EG: api crawlers
	pl = &Planet{}
	err = DB.C(collection).FindByID(p.SWID, pl)
	return
}

func (p *Planet) GetByName() (pl *Planet, err error) {
	pl = &Planet{}
	err = DB.C(collection).FindByName(p.Name, pl)
	return
}

func (p *Planet) Create() (err error) {
	p.UUID = uuid.New().String()
	return DB.C(collection).Insert(p)
}

func (p *Planet) Delete() (err error) {
	return DB.C(collection).Delete(p.SWID)
}

func allListInMap(planetName, listName string, list []string, m map[string]string) (ok bool, errS string) {
	for _, l := range list {
		l = strings.Replace(l, " ", "", -1)
		_, ok = m[l]
		if !ok {
			errS = fmt.Sprintf("%s '%s' not registered for planet '%s'", listName, l, planetName)
			return
		}
	}
	return
}
