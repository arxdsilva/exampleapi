package main

import (
	"log"
	"os"

	"github.com/arxdsilva/exampleapi/api"
	"github.com/arxdsilva/exampleapi/config"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func init() {
	config.Load()
}

func main() {
	e := echo.New()
	// add possible Middlewares
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// Routes
	e.GET("/", api.HealthCheck)
	e.POST("/planet/new", api.CreatePlanet)
	e.GET("/planets", api.GetAllPlanets)
	e.GET("/planets/:id", api.GetPlanetByID)
	e.GET("/planets/name", api.GetPlanetByName)
	e.DELETE("/planets/:id", api.DeletePlanet)
	// Start server
	log.Println("Starting app in port: ", port())
	e.Logger.Fatal(e.Start(port()))
}

func port() string {
	p := os.Getenv("PORT")
	if p != "" {
		return ":" + p
	}
	return ":1323"
}
