package api

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/arxdsilva/exampleapi/config"
	"github.com/arxdsilva/exampleapi/db"
	"github.com/arxdsilva/exampleapi/models"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func init() {
	config.LoadMock()
}

func TestCreatePlanet_BindErr(t *testing.T) {
	planetJSON := `[]`
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(planetJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	assert.Error(t, CreatePlanet(c))
}

func TestCreatePlanet_Check_OK(t *testing.T) {
	planetID := "1"
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/planets/"+planetID+"/" {
				w.Header().Add("Content-Type", "application/json")
				json := `{
					"name": "Tatooine",
					"rotation_period": "23",
					"orbital_period": "304",
					"diameter": "10465",
					"climate": "arid",
					"gravity": "1 standard",
					"terrain": "desert",
					"surface_water": "1",
					"population": "200000",
					"residents": ["https://swapi.co/api/people/1/", "https://swapi.co/api/people/2/", "https://swapi.co/api/people/4/", "https://swapi.co/api/people/6/", "https://swapi.co/api/people/7/", "https://swapi.co/api/people/8/", "https://swapi.co/api/people/9/", "https://swapi.co/api/people/11/", "https://swapi.co/api/people/43/", "https://swapi.co/api/people/62/"],
					"films": ["https://swapi.co/api/films/5/", "https://swapi.co/api/films/4/", "https://swapi.co/api/films/6/", "https://swapi.co/api/films/3/", "https://swapi.co/api/films/1/"],
					"created": "2014-12-09T13:50:49.641000Z",
					"edited": "2014-12-21T20:48:04.175778Z",
					"url": "https://swapi.co/api/planets/1/"
				}`
				w.Write([]byte(json))
			}
		}),
	)
	os.Setenv("SWAPI_URL", ts.URL)
	defer os.Unsetenv("SWAPI_URL")
	planetJSON := `{"name":"Tatooine", "climate":"arid", "terrain":"desert"}`
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(planetJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	assert.Nil(t, CreatePlanet(c))
	assert.Equal(t, http.StatusOK, rec.Code)
}

func TestCreatePlanet_Name_Err(t *testing.T) {
	planetJSON := `{"climate":"arid", "terrain":"desert"}`
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(planetJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err := CreatePlanet(c)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "have a name")
}

func TestCreatePlanet_Climate_Err(t *testing.T) {
	planetJSON := `{"name":"Tatooine", "terrain":"desert"}`
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(planetJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err := CreatePlanet(c)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "have a climate")
}

func TestCreatePlanet_Terrain_Err(t *testing.T) {
	planetJSON := `{"name":"Tatooine", "climate":"arid"}`
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(planetJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err := CreatePlanet(c)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "have a terrain")
}

func TestGetAllPlanets_OK(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	assert.Nil(t, GetAllPlanets(c))
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Contains(t, rec.Body.String(), "Tatooine")
}

func TestGetPlanetByID_ErrID(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("error")
	err := GetPlanetByID(c)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "not a valid ID")
}

func TestGetPlanetByID_ErrNotID(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("333")
	err := GetPlanetByID(c)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "not valid")
}

func TestGetPlanetByID_OK(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("30")
	err := GetPlanetByID(c)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, rec.Code)
}

func TestGetPlanetByID_ErrNotFound(t *testing.T) {
	models.DB = db.NewMockSession2().DB("exampleapi")
	defer config.LoadMock()
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("3")
	err := GetPlanetByID(c)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "not yet registered")
}

func TestGetPlanetByName_OK(t *testing.T) {
	models.DB = db.NewMockSession2().DB("exampleapi")
	defer config.LoadMock()
	e := echo.New()
	q := make(url.Values)
	q.Set("name", "Tatooine")
	req := httptest.NewRequest(http.MethodGet, "/?"+q.Encode(), nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err := GetPlanetByName(c)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Contains(t, rec.Body.String(), "Tatooine2")
}

func TestDeletePlanet_OK(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("30")
	err := DeletePlanet(c)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNoContent, rec.Code)
}

func TestDeletePlanet_DBErr(t *testing.T) {
	models.DB = db.NewMockSession2().DB("exampleapi")
	defer config.LoadMock()
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("30")
	err := DeletePlanet(c)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "Could not delete ")
}

func TestDeletePlanet_IDErr(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("303")
	err := DeletePlanet(c)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "is not valid")
}

func TestDeletePlanet_Invalid_IDErr(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/planets/:id")
	c.SetParamNames("id")
	c.SetParamValues("abs")
	err := DeletePlanet(c)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "is not a valid")
}

func TestGetAllPlanets_NoResult(t *testing.T) {
	models.DB = db.NewMockSession2().DB("exampleapi")
	defer config.LoadMock()
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err := GetAllPlanets(c)
	assert.Nil(t, err)
	assert.Equal(t, "[]\n", rec.Body.String())
}
