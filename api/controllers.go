package api

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/arxdsilva/exampleapi/models"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2"
)

func HealthCheck(c echo.Context) (err error) { return c.String(http.StatusOK, "OK") }

// CreatePlanet adds a planet to DB
// RESPONSES:
// 200 OK
// 400 Bad Request
// 500 Internal Error
func CreatePlanet(c echo.Context) (err error) {
	p := new(models.Planet)
	err = c.Bind(p)
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusBadRequest, "Could not bind request body to Planet structure")
	}
	err = p.Check()
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	pl, err := p.GetByName()
	if (err != nil) && (err != mgo.ErrNotFound) {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if pl.URL != "" {
		alreadyInserted := fmt.Sprintf("Planet '%s' has alredy been inserted into DB", p.Name)
		return echo.NewHTTPError(http.StatusConflict, alreadyInserted)
	}
	// the best way to deal with these infos and garantee a low latency response
	// is to first just create and then put it into a worker line to get updated
	// this way it'll have all infos and prevent responding 500 if SWAPI
	// has some kind of trouble
	err = p.FetchPlanetInfos()
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Could not create planet")
	}
	err = p.Create()
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Could not create planet")
	}
	return c.JSON(http.StatusOK, p)
}

// GetAllPlanets fetches all added planets from DB
// RESPONSES:
// 200 OK
// 500 Internal Error
func GetAllPlanets(c echo.Context) (err error) {
	p := new(models.Planet)
	ps, err := p.GetAll()
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Could not fetch planets")
	}
	// avoid null response
	if len(ps) == 0 {
		ps = []models.Planet{}
	}
	return c.JSON(http.StatusOK, ps)
}

// GetPlanetByID fetches a planet from DB
// RESPONSES:
// 200 OK
// 400 Bad request
// 404 Not found
// 500 Internal Error
func GetPlanetByID(c echo.Context) (err error) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusBadRequest,
			c.Param("id")+" is not a valid ID")
	}
	// check if ID is in the swapi range
	p := new(models.Planet)
	p.SWID = id
	if exists := p.ExistsID(); !exists {
		return echo.NewHTTPError(http.StatusBadRequest, "ID is not valid")
	}
	pl, err := p.GetByID()
	if (err != nil) && (err != mgo.ErrNotFound) {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Could not fetch planet by ID")
	}
	// check for empty db response
	if pl.URL == "" {
		return echo.NewHTTPError(http.StatusNotFound, fmt.Sprintf("Planet ID %v not yet registered", id))
	}
	return c.JSON(http.StatusOK, pl)
}

// GetPlanetByName fetches a planet from DB
// RESPONSES:
// 200 OK
// 400 Bad request
// 404 Not found
// 500 Internal Error
func GetPlanetByName(c echo.Context) (err error) {
	name := c.QueryParam("name")
	p := new(models.Planet)
	p.Name = name
	if exist := p.ExistsName(); !exist {
		return echo.NewHTTPError(http.StatusBadRequest,
			fmt.Sprintf("Planet %s does not exist in SW universe", name))
	}
	pl, err := p.GetByName()
	if (err != nil) && (err != mgo.ErrNotFound) {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Could not fetch planet by Name")
	}
	// check for empty db response
	if pl.URL == "" {
		return echo.NewHTTPError(http.StatusNotFound, fmt.Sprintf("Planet %v not yet registered", name))
	}
	return c.JSON(http.StatusOK, pl)
}

// DeletePlanet deletes a planet from DB
// RESPONSES:
// 204 No Content
// 400 Bad request
// 500 Internal Error
func DeletePlanet(c echo.Context) (err error) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusBadRequest,
			c.Param("id")+" is not a valid ID")
	}
	// check if ID is in the swapi range
	p := new(models.Planet)
	p.SWID = id
	if exists := p.ExistsID(); !exists {
		return echo.NewHTTPError(http.StatusBadRequest, "ID is not valid")
	}
	err = p.Delete()
	if err != nil {
		log.Println("[err]: ", err)
		return echo.NewHTTPError(http.StatusInternalServerError,
			fmt.Sprintf("Could not delete planet ID %v", id))
	}
	return c.NoContent(http.StatusNoContent)
}
