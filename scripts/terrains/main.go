package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/arxdsilva/exampleapi/models"
)

type Planets struct {
	Count    int             `json:"count"`
	Next     interface{}     `json:"next"`
	Previous interface{}     `json:"previous"`
	Results  []models.Planet `json:"results"`
}

var terrains map[string]string

func main() {
	ps, err := getPlanets()
	if err != nil {
		log.Fatal(err)
	}
	extractTerrains(ps)
}

func extractTerrains(ps []models.Planet) {
	// extract all terrains from planets
	var terrainsBuff []string
	for _, planet := range ps {
		terrainsBuff = append(terrainsBuff, strings.Split(planet.Terrain, ",")...)
	}
	// strip whitespace separator
	var terrainsB []string
	for i, c := range terrainsBuff {
		if i > 0 {
			c = strings.Replace(c, " ", "", 1)
		}
		terrainsB = append(terrainsB, c)
	}
	// only get unique terrains
	terrains = make(map[string]string)
	for _, c := range terrainsB {
		_, ok := terrains[c]
		if !ok {
			terrains[c] = ""
		}
	}
	for t, _ := range terrains {
		fmt.Println(t)
	}
	return
}

func getPlanets() (ps []models.Planet, err error) {
	i := 1
	for {
		url := fmt.Sprintf("https://swapi.co/api/planets/?page=%v", i)
		resp, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		respJSON := Planets{}
		err = json.NewDecoder(resp.Body).Decode(&respJSON)
		if err != nil {
			log.Fatal(err)
		}
		ps = append(ps, respJSON.Results...)
		if (respJSON.Next == "") || (respJSON.Next == nil) {
			break
		}
		i++
	}
	return
}
