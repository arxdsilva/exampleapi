package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strconv"

	"github.com/arxdsilva/exampleapi/models"
)

type Planets struct {
	Count    int             `json:"count"`
	Next     interface{}     `json:"next"`
	Previous interface{}     `json:"previous"`
	Results  []models.Planet `json:"results"`
}

func main() {
	ps, err := getPlanets()
	if err != nil {
		log.Fatal(err)
	}
	// get only the names
	names := make(map[string]int)
	idMap := make(map[int]string)
	for _, p := range ps {
		_, ok := names[p.Name]
		if !ok {
			id, errID := planetID(p.URL)
			if errID != nil {
				log.Fatal(errID)
			}
			names[p.Name] = id
			idMap[id] = p.Name
		}
	}
	fmt.Println("NAMES: ", len(names))
	for k, v := range names {
		fmt.Printf(`"%s":%v, `, k, v)
	}
	fmt.Println("\nIDs: ", len(idMap))
	for k, v := range idMap {
		fmt.Printf(`%v:"%s", `, k, v)
	}
}

func getPlanets() (ps []models.Planet, err error) {
	i := 1
	for {
		url := fmt.Sprintf("https://swapi.co/api/planets/?page=%v", i)
		resp, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		respJSON := Planets{}
		err = json.NewDecoder(resp.Body).Decode(&respJSON)
		if err != nil {
			log.Fatal(err)
		}
		ps = append(ps, respJSON.Results...)
		if (respJSON.Next == "") || (respJSON.Next == nil) {
			break
		}
		i++
	}
	return
}

func planetID(url string) (id int, err error) {
	re := regexp.MustCompile(`\d+`)
	idS := re.FindString(url)
	return strconv.Atoi(idS)
}
