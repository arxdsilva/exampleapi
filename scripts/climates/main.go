package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/arxdsilva/exampleapi/models"
)

type Planets struct {
	Count    int             `json:"count"`
	Next     interface{}     `json:"next"`
	Previous interface{}     `json:"previous"`
	Results  []models.Planet `json:"results"`
}

var climates map[string]string

func main() {
	ps, err := getPlanets()
	if err != nil {
		log.Fatal(err)
	}
	extractClimates(ps)
}

func extractClimates(ps []models.Planet) {
	// extract all climates from planets
	var climatesBuff []string
	for _, planet := range ps {
		climatesBuff = append(climatesBuff, strings.Split(planet.Climate, ",")...)
	}
	// strip whitespace
	var climatesB []string
	for _, c := range climatesBuff {
		c = strings.Replace(c, " ", "", -1)
		climatesB = append(climatesB, c)
	}
	// only get unique climates
	climates = make(map[string]string)
	for _, c := range climatesB {
		_, ok := climates[c]
		if !ok {
			climates[c] = ""
		}
	}
	fmt.Printf("%+v", climates)
	return
}

func getPlanets() (ps []models.Planet, err error) {
	i := 1
	for {
		url := fmt.Sprintf("https://swapi.co/api/planets/?page=%v", i)
		resp, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		respJSON := Planets{}
		err = json.NewDecoder(resp.Body).Decode(&respJSON)
		if err != nil {
			log.Fatal(err)
		}
		ps = append(ps, respJSON.Results...)
		if (respJSON.Next == "") || (respJSON.Next == nil) {
			break
		}
		i++
	}
	return
}
